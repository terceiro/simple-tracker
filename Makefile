mirror = $(shell ./get-mirror)

trackers = $(wildcard trackers/*.sh)
html = $(patsubst trackers/%.sh, public/%/index.html, $(trackers))
assets = public/.assets/bootstrap public/.assets/chart.js public/.assets/jquery.flot.js public/.assets/jquery.js public/.assets/jquery.tablesorter.js

all: $(html) $(assets)

-include conf.mk

$(html): public/%/index.html : trackers/%.sh template.html.erb Packages Sources
	mkdir -p $$(dirname $@)
	./expand template.html.erb trackers/$*.sh

Packages:
	wget --progress=dot -O $@.xz $(mirror)/dists/unstable/main/binary-amd64/Packages.xz
	unxz $@.xz

Sources:
	wget --progress=dot -O $@.xz $(mirror)/dists/unstable/main/source/Sources.xz
	unxz $@.xz

public/.assets/bootstrap:
	mkdir -p $$(dirname $@)
	ln -sfT /usr/share/javascript/bootstrap $@

public/.assets/chart.js: chart.js
	mkdir -p $$(dirname $@)
	cp $< $@

public/.assets/jquery.js:
	mkdir -p $$(dirname $@)
	ln -sfT /usr/share/javascript/jquery/jquery.min.js $@

public/.assets/jquery.flot.js:
	mkdir -p $$(dirname $@)
	ln -sfT /usr/share/javascript/jquery-flot/jquery.flot.min.js $@

public/.assets/jquery.tablesorter.js:
	mkdir -p $$(dirname $@)
	ln -sfT /usr/share/javascript/jquery-tablesorter/jquery.tablesorter.min.js $@

quiet-reload:
	@mkdir -p log
	@log=log/$$(date +%d).log; \
		$(MAKE) reload > $$log 2>&1 || cat $$log

reload: cleandata
	$(MAKE)

cleandata:
	$(RM) Packages* Sources*

clean: cleandata
	$(RM) -r public/
