function to_date_string(date) {
  return date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
}

function to_proper_date(date_as_number) {
  var year = Math.floor(date_as_number / 10000);
  var month = Math.floor(date_as_number % 10000 / 100);
  var day = Math.floor(date_as_number % 100);
  return new Date(year, month - 1, day);
}

$(function() {
  $.getJSON('data.json', function(input) {

    var packages = [];
    var initial_date = null;
    var final_date = null;

    $.each(input, function(index, point) {
      var date = to_proper_date(point.date);
      packages.push([date.getTime(), point.packages]);
      if (initial_date == null || date < initial_date) { initial_date = date; }
      if (final_date == null || date > final_date) { final_date = date; }
    });

    $.plot(
      $('#progress'),
      [
        { label: "Packages", data: packages, color: "#a40000" },
      ],
      {
        series: { points: { show: true }, lines: { show: true } },
        grid: { hoverable: true },
        yaxis: {
          min: 0
        },
        xaxis: {
          min: initial_date.getTime(),
          max: final_date.getTime() + 1000*60*60*24, // always leave some empty space in the chart, eqivalent to 1 day
          ticks: [[initial_date, to_date_string(initial_date)], [final_date, to_date_string(final_date)]]
        }
      }
    );

    // based on http://people.iola.dk/olau/flot/examples/interacting.html
    function showTooltip(x, y, contents) {
      $('<div id="tooltip">' + contents + '</div>').css( {
        position: 'absolute',
        display: 'none',
        top: y - 30,
        left: x + 10,
        border: '1px solid #c4a000',
        padding: '2px',
        'background-color': '#fce94f',
        opacity: 0.80
      }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#progress").bind("plothover", function (event, pos, item) {
      if (item) {
        if (previousPoint != item.dataIndex) {
          previousPoint = item.dataIndex;

          $("#tooltip").remove();
          var date = new Date(item.datapoint[0]);
          var packages = item.datapoint[1];
          showTooltip(item.pageX, item.pageY, to_date_string(date) + ': ' + packages)
        }
      }
      else {
        $("#tooltip").remove();
        previousPoint = null;
      }
    });


  });
});
